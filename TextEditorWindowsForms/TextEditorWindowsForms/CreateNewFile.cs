﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TextEditorWindowsForms
{
    public partial class CreateNewFile : Form
    {
        private MainForm _mainForm;
        public CreateNewFile()
        {
            InitializeComponent();
        }
        public CreateNewFile(MainForm form)
        {
            InitializeComponent();
            _mainForm = form;
        }
        private void createTable()
        {
            SQLiteCommand sqCommand = new SQLiteCommand(
                    "CREATE TABLE FILE (ID INT PRIMARY KEY, AUTHOR VARCHAR(20), TEXT VARCHAR(1000));",
                    MainForm.SqConnection);
            MainForm.SqConnection.Open();
            sqCommand.ExecuteNonQuery();
            MainForm.SqConnection.Close();
        }
            
        private void btnSaveToDB_Click(object sender, EventArgs e)
        {
            if (this.tBoxAuthor.Text!=string.Empty
                && this.tBoxText.Text != string.Empty)
            {
                using (SQLiteConnection SqConnection = new SQLiteConnection(MainForm.SqConnection))
                {
                    string sqRequest = "INSERT INTO FILE (AUTHOR,TEXT) VALUES (@author, @text)";
                    SQLiteCommand sqCommand = new SQLiteCommand(sqRequest, SqConnection);
                    sqCommand.Parameters.AddWithValue("@author", this.tBoxAuthor.Text);
                    sqCommand.Parameters.AddWithValue("@text", this.tBoxText.Text);
                    try
                    {
                        SqConnection.Open();
                        sqCommand.ExecuteNonQuery();
                        SqConnection.Close();
                        _mainForm.DgvLoad();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }
                this.Close();
            }
            else
            {
                MessageBox.Show("You must fill Author and Text");
            } 
        }

        private async void btnSaveToDBAsync_Click(object sender, EventArgs e)
        {
            if (this.tBoxAuthor.Text != string.Empty
               && this.tBoxText.Text != string.Empty)
            {
                using (SQLiteConnection SqConnection = new SQLiteConnection(MainForm.SqConnection))
                {
                    SQLiteCommand sqCommand = SqConnection.CreateCommand();
                    sqCommand.CommandText = "INSERT INTO FILE (AUTHOR,TEXT) VALUES (@author, @text)";
                    sqCommand.Parameters.AddWithValue("@author", this.tBoxAuthor.Text);
                    sqCommand.Parameters.AddWithValue("@text", this.tBoxText.Text);
                    SqConnection.Open();
                    await ExecuteNonQueryAsync(SqConnection);
                    try
                    {                      
                        Task.WaitAll();                       
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                    SqConnection.Close();
                }             
                _mainForm.DgvLoad();
                this.Close();
            }
        }

        private async Task ExecuteNonQueryAsync(SQLiteConnection SqConnection)
        {
            SQLiteCommand sqCommand = SqConnection.CreateCommand();
            sqCommand.CommandText = "INSERT INTO FILE (AUTHOR,TEXT) VALUES (@author, @text)";
            sqCommand.Parameters.AddWithValue("@author", this.tBoxAuthor.Text);
            sqCommand.Parameters.AddWithValue("@text", this.tBoxText.Text);
            await sqCommand.ExecuteNonQueryAsync();
        }
    }
}
