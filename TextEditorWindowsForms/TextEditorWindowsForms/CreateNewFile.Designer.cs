﻿namespace TextEditorWindowsForms
{
    partial class CreateNewFile
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tBoxAuthor = new System.Windows.Forms.TextBox();
            this.tBoxText = new System.Windows.Forms.TextBox();
            this.btnSaveToDB = new System.Windows.Forms.Button();
            this.btnSaveToDBAsync = new System.Windows.Forms.Button();
            this.lblInputAuthor = new System.Windows.Forms.Label();
            this.lblInputText = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // tBoxAuthor
            // 
            this.tBoxAuthor.Location = new System.Drawing.Point(164, 33);
            this.tBoxAuthor.Name = "tBoxAuthor";
            this.tBoxAuthor.Size = new System.Drawing.Size(100, 20);
            this.tBoxAuthor.TabIndex = 0;
            // 
            // tBoxText
            // 
            this.tBoxText.Location = new System.Drawing.Point(69, 90);
            this.tBoxText.Multiline = true;
            this.tBoxText.Name = "tBoxText";
            this.tBoxText.Size = new System.Drawing.Size(294, 164);
            this.tBoxText.TabIndex = 1;
            // 
            // btnSaveToDB
            // 
            this.btnSaveToDB.Location = new System.Drawing.Point(69, 286);
            this.btnSaveToDB.Name = "btnSaveToDB";
            this.btnSaveToDB.Size = new System.Drawing.Size(75, 23);
            this.btnSaveToDB.TabIndex = 2;
            this.btnSaveToDB.Text = "Save to DB";
            this.btnSaveToDB.UseVisualStyleBackColor = true;
            this.btnSaveToDB.Click += new System.EventHandler(this.btnSaveToDB_Click);
            // 
            // btnSaveToDBAsync
            // 
            this.btnSaveToDBAsync.Location = new System.Drawing.Point(245, 286);
            this.btnSaveToDBAsync.Name = "btnSaveToDBAsync";
            this.btnSaveToDBAsync.Size = new System.Drawing.Size(118, 23);
            this.btnSaveToDBAsync.TabIndex = 3;
            this.btnSaveToDBAsync.Text = "Save to DB Async";
            this.btnSaveToDBAsync.UseVisualStyleBackColor = true;
            this.btnSaveToDBAsync.Click += new System.EventHandler(this.btnSaveToDBAsync_Click);
            // 
            // lblInputAuthor
            // 
            this.lblInputAuthor.AutoSize = true;
            this.lblInputAuthor.Location = new System.Drawing.Point(161, 9);
            this.lblInputAuthor.Name = "lblInputAuthor";
            this.lblInputAuthor.Size = new System.Drawing.Size(106, 13);
            this.lblInputAuthor.TabIndex = 4;
            this.lblInputAuthor.Text = "Input Author\'s Name:";
            // 
            // lblInputText
            // 
            this.lblInputText.AutoSize = true;
            this.lblInputText.Location = new System.Drawing.Point(182, 74);
            this.lblInputText.Name = "lblInputText";
            this.lblInputText.Size = new System.Drawing.Size(58, 13);
            this.lblInputText.TabIndex = 5;
            this.lblInputText.Text = "Input Text:";
            this.lblInputText.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // CreateNewFile
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(430, 324);
            this.Controls.Add(this.lblInputText);
            this.Controls.Add(this.lblInputAuthor);
            this.Controls.Add(this.btnSaveToDBAsync);
            this.Controls.Add(this.btnSaveToDB);
            this.Controls.Add(this.tBoxText);
            this.Controls.Add(this.tBoxAuthor);
            this.Name = "CreateNewFile";
            this.Text = "CreateNewFile";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tBoxAuthor;
        private System.Windows.Forms.TextBox tBoxText;
        private System.Windows.Forms.Button btnSaveToDB;
        private System.Windows.Forms.Button btnSaveToDBAsync;
        private System.Windows.Forms.Label lblInputAuthor;
        private System.Windows.Forms.Label lblInputText;
    }
}