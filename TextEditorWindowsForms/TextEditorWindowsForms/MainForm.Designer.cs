﻿namespace TextEditorWindowsForms
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dGView = new System.Windows.Forms.DataGridView();
            this.btnEditFile = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.btnAddNewFile = new System.Windows.Forms.Button();
            this.btnRefreshAsync = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dGView)).BeginInit();
            this.SuspendLayout();
            // 
            // dGView
            // 
            this.dGView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dGView.Location = new System.Drawing.Point(224, 69);
            this.dGView.Name = "dGView";
            this.dGView.Size = new System.Drawing.Size(142, 275);
            this.dGView.TabIndex = 0;
            // 
            // btnEditFile
            // 
            this.btnEditFile.Location = new System.Drawing.Point(413, 153);
            this.btnEditFile.Name = "btnEditFile";
            this.btnEditFile.Size = new System.Drawing.Size(75, 23);
            this.btnEditFile.TabIndex = 3;
            this.btnEditFile.Text = "Edit";
            this.btnEditFile.UseVisualStyleBackColor = true;
            this.btnEditFile.Click += new System.EventHandler(this.btnEditFile_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(413, 246);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(75, 23);
            this.btnDelete.TabIndex = 4;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(274, 53);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "List of Files";
            // 
            // btnAddNewFile
            // 
            this.btnAddNewFile.Location = new System.Drawing.Point(234, 360);
            this.btnAddNewFile.Name = "btnAddNewFile";
            this.btnAddNewFile.Size = new System.Drawing.Size(113, 23);
            this.btnAddNewFile.TabIndex = 6;
            this.btnAddNewFile.Text = "Add New File";
            this.btnAddNewFile.UseVisualStyleBackColor = true;
            this.btnAddNewFile.Click += new System.EventHandler(this.btnAddNewFile_Click);
            // 
            // btnRefreshAsync
            // 
            this.btnRefreshAsync.Location = new System.Drawing.Point(210, 12);
            this.btnRefreshAsync.Name = "btnRefreshAsync";
            this.btnRefreshAsync.Size = new System.Drawing.Size(178, 23);
            this.btnRefreshAsync.TabIndex = 7;
            this.btnRefreshAsync.Text = "Refresh data async";
            this.btnRefreshAsync.UseVisualStyleBackColor = true;
            this.btnRefreshAsync.Click += new System.EventHandler(this.btnRefreshAsync_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(629, 426);
            this.Controls.Add(this.btnRefreshAsync);
            this.Controls.Add(this.btnAddNewFile);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnEditFile);
            this.Controls.Add(this.dGView);
            this.Name = "MainForm";
            this.Text = "MainForm";
            ((System.ComponentModel.ISupportInitialize)(this.dGView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dGView;
        private System.Windows.Forms.Button btnEditFile;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnAddNewFile;
        private System.Windows.Forms.Button btnRefreshAsync;
    }
}