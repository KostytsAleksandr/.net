﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TextEditorWindowsForms
{
    public partial class EditFile : Form
    {
        private MainForm _mainForm;
        private string _selectedAuthor;
        public EditFile()
        {
            InitializeComponent();
        }
        public EditFile(MainForm form, string selectedAuthor,string selectedText)
        {
            InitializeComponent();
            _mainForm = form;
            _selectedAuthor = selectedAuthor;
            lblAuthor.Text = _selectedAuthor;
            tBoxEditText.Text = selectedText;
        }

        private void btnSaveChanges_Click(object sender, EventArgs e)
        {
            if (this.tBoxEditText.Text != string.Empty)
            {
                using (SQLiteConnection SqConnection = new SQLiteConnection(MainForm.SqConnection))
                {
                    string sqRequest = "UPDATE FILE SET TEXT = @text WHERE AUTHOR = @author";
                    SQLiteCommand sqCommand = new SQLiteCommand(sqRequest, SqConnection);
                    sqCommand.Parameters.AddWithValue("@author", _selectedAuthor);
                    sqCommand.Parameters.AddWithValue("@text", this.tBoxEditText.Text);
                    try
                    {
                        SqConnection.Open();
                        sqCommand.ExecuteNonQuery();
                        SqConnection.Close();
                        _mainForm.DgvLoad();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }
                this.Close();
            }
            else
            {
                MessageBox.Show("You must fill Author and Text");
            }
        }
    }
}
