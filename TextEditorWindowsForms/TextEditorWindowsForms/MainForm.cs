﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TextEditorWindowsForms
{
    public partial class MainForm : Form
    {
        public static string DatabaseName { get; set; }
        public static SQLiteConnection SqConnection { get; set; }
        public MainForm()
        {
            InitializeComponent();
            DatabaseName = @"d:\MyData\FileRedactor.db";
            SqConnection = new SQLiteConnection($"Data Source={DatabaseName}");
            if (!File.Exists(DatabaseName))
            {
                CreateDatabaseAndTable();
            }
            DgvLoad();
        }

        public void DgvLoad()
        {            
            using (SQLiteConnection SqLiteConnection = new SQLiteConnection(SqConnection))
            {
                string sqRequest = "SELECT AUTHOR FROM FILE";
                SQLiteDataAdapter sqDataAdapter = new SQLiteDataAdapter(sqRequest, SqLiteConnection);
                SQLiteCommandBuilder commandBuilder = new SQLiteCommandBuilder(sqDataAdapter);
                DataSet ds = new DataSet();
                try
                {
                    sqDataAdapter.Fill(ds);
                    dGView.DataSource = ds.Tables[0];

                }
                catch(Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
           
        }

        public string SelectTextFromDB()
        {
            string text = string.Empty;
            using (SQLiteConnection SqLiteConnection = new SQLiteConnection(SqConnection))
            {
                string sqRequest = "SELECT TEXT FROM FILE WHERE AUTHOR = @author";
                SQLiteCommand sqCommand = new SQLiteCommand(sqRequest, SqLiteConnection);
                string selectedAuthor= this.dGView.SelectedCells[0].Value.ToString();
                sqCommand.Parameters.AddWithValue("@author", selectedAuthor);
                try
                {
                    SqLiteConnection.Open();
                    text=sqCommand.ExecuteScalar().ToString();
                    SqLiteConnection.Close();

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            return text;
        }

        private void CreateDatabaseAndTable()
        {
            using (SQLiteConnection SqLiteConnection=new SQLiteConnection(SqConnection))
            {
                string sqRequest = "CREATE TABLE FILE (ID INTEGER PRIMARY KEY AUTOINCREMENT, AUTHOR VARCHAR(20), TEXT VARCHAR(1000));";
                SQLiteCommand sqCommand = new SQLiteCommand(sqRequest, SqLiteConnection);
                try
                {
                    SQLiteConnection.CreateFile(DatabaseName);
                    SqLiteConnection.Open();
                    sqCommand.ExecuteNonQuery();
                    SqLiteConnection.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }                      
        }

        private void btnAddNewFile_Click(object sender, EventArgs e)
        {
            CreateNewFile newFile = new CreateNewFile(this);
            newFile.Show();
        }

        private void btnEditFile_Click(object sender, EventArgs e)
        {
            string authorForEditing = this.dGView.SelectedCells[0].Value.ToString();
            EditFile editFile = new EditFile(this, authorForEditing, SelectTextFromDB());
            editFile.Show();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {

            using (SQLiteConnection SqLiteConnection = new SQLiteConnection(SqConnection))
            {
                string sqRequest = "DELETE FROM FILE WHERE AUTHOR = @author ;";
                SQLiteCommand sqCommand = new SQLiteCommand(sqRequest, SqLiteConnection);
                string authorForDeleting = this.dGView.SelectedCells[0].Value.ToString();
                sqCommand.Parameters.AddWithValue("@author", authorForDeleting);
                try
                {
                    SqLiteConnection.Open();
                    sqCommand.ExecuteNonQuery();
                    SqLiteConnection.Close();
                    DgvLoad();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private async void btnRefreshAsync_Click(object sender, EventArgs e)
        {
            SQLiteConnection sQLiteConnection = new SQLiteConnection(SqConnection);
            sQLiteConnection.Open();
            DataSet ds = new DataSet();
            ds = await ExecuteAsync();
            dGView.DataSource = ds.Tables[0];
            sQLiteConnection.Close();
        }

        private async Task<DataSet> ExecuteAsync()
        {
                return await Task.Run(() =>
                 {
                     DataSet ds = new DataSet();
                     SQLiteConnection sQLiteConnection = new SQLiteConnection(SqConnection);
                     SQLiteCommand sqCommand = sQLiteConnection.CreateCommand();
                     sqCommand.CommandText = "SELECT AUTHOR FROM FILE";
                     var da = new SQLiteDataAdapter(sqCommand);
                     da.Fill(ds, "AUTHOR");
                     return ds;
                 });
        }
    }
}
